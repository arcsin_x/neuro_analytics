import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from typing import List, Dict, Tuple

def load_results(path_to_file:str) -> Dict[str, List[int]]:
    """
    Возвращает pd.DataFrame с ключами:
    prediction - результат предсказания классификатора [0; 1]
    ground_true - истинное значение класса [0; 1]
    confidence - уверенность предсказания [0, 1]
    """
    with open(path_to_file, 'rb') as pickled:
        full_data = pickle.load(pickled)

    data = full_data[0]
    table = {
        'prediction': [], 
        'ground_true': [],
        'confidence': []
        }

    for row in data:
        table['prediction'].append(row[0])
        table['ground_true'].append(row[1])
        table['confidence'].append(row[2].item())
    
    df = pd.DataFrame(table)

    return df

def split_data(df):
    """
    #TODO Для мультиклассового классификатора

    Возвращает список словарей, где каждый список - 
    результаты предсказаний для одного класса.
    """
    classes = np.unique(df['ground_true']).tolist()
    #print(classes)
    
    df_list = []
    for num_class in classes:
        df_list.append(df.loc[(df.ground_true == num_class)])

    return df_list

def change_value(df, tresh):
    """
    Изменяет значения в df['prediction'] на те, которые
    будут предсказаны классификатором при заданном пороге.
    """
    for i in range(0, len(df)):
        if df['prediction'][i] == 0:
            if df['confidence'][i] >= tresh:
                df['prediction'][i] = 0
            else:
                df['prediction'][i] = 1
        else:
            if df['confidence'][i] >= tresh:
                df['prediction'][i] = 1
            else:
                df['prediction'][i] = 1
    
    return df

def calculate_value(df: pd.DataFrame, tresh=0.9):
    """
    Возвращает prescision and recall для всего датафрейма
    с учетом этих значений.
    """
    #TODO разные пороги для разных классов
    df = change_value(df, tresh)

    tp = len(df.loc[(df.prediction == 1) & (df.ground_true == 1)])
    fp = len(df.loc[(df.prediction == 1) & (df.ground_true == 0)])
    fn = len(df.loc[(df.prediction == 0) & (df.ground_true == 1)])
    tn = len(df.loc[(df.prediction == 0) & (df.ground_true == 0)])

    pre = tp/(tp+fp) if (tp+fp != 0) else np.nan
    rec = tp/(tp+fn) if (tp+fn != 0) else np.nan

    #print(pre, rec)
    #print(tp, fp, fn, tn)

    return pre, rec

def treshold_steps(dataframe, step):
    prescision = []
    recall = []

    for val in np.linspace(0,1,111).tolist():
        pre, rec = calculate_value(df, tresh=val)
        prescision.append(pre)
        recall.append(rec)

    return prescision, recall

def vis_plot(prescision, recall):
    plt.plot(prescision, recall)
    plt.xlabel('Prescision')
    plt.ylabel('Recall')
    plt.show()


df = load_results('weird_data.pickle')
#df_list = split_data(df)
#pre, rec = calculate_value(df, tresh=0.99)
prescision, recall = treshold_steps(df, 0.1)
vis_plot(prescision, recall)